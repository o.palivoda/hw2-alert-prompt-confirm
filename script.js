// Які існують типи даних у Javascript?
// 1. Number, boolean, string, bigInt, null, undefined, Object, Symbol.

// У чому різниця між == і ===?

// 2. Подвійне дорівнює мне враховує перетворення типів. Наприклад, 1 == '1' буде вірно. При потрійному дорівнює буде false.

// Що таке оператор?

// 3. оператори - це символи, які використовуються для відповідних операцій з операндами.



// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

const name = prompt('What is your name?');
if (name == '' || name == undefined) {
    prompt('What is your name?');
}
const age = +prompt('What is your age?');

if (Number.isNaN(age)) {
    +prompt('What is your age?')
} else if (age < 18) {
    alert('You are not allowed to visit this website')
} else if (age >= 18 && age <= 22) {
    const result = confirm('Are you sure you want to continue?')
    if (result) {
        alert(`Welcome, ${name}`);
    } else {
        alert('You are not allowed to visit this website')
    }
} else if (age > 22) {
    alert(`Welcome, ${name}`)
}